/*
 * Digests in exit_lists has to be created programmatically.
 */

CREATE TABLE IF NOT EXISTS exit_list(
   downloaded                TIMESTAMP WITHOUT TIME ZONE   NOT NULL,
   header                    TEXT                          NOT NULL,
   digest                    TEXT                          NOT NULL,
   PRIMARY KEY(digest)
);

CREATE TABLE IF NOT EXISTS exit_node(
   published                 TIMESTAMP WITHOUT TIME ZONE   NOT NULL,
   last_status               TIMESTAMP WITHOUT TIME ZONE   NOT NULL,
   fingerprint               TEXT                          NOT NULL,
   exit_addresses            TEXT                          NOT NULL,
   exit_addresses_timestamps TEXT                          NOT NULL,
   exit_list                 TEXT references exit_list(digest)
);
