package org.torproject.metrics.descriptorparser.parsers;

import org.torproject.descriptor.BandwidthHistory;
import org.torproject.descriptor.BridgeExtraInfoDescriptor;
import org.torproject.descriptor.Descriptor;
import org.torproject.descriptor.DescriptorReader;
import org.torproject.descriptor.DescriptorSourceFactory;
import org.torproject.descriptor.ExtraInfoDescriptor;
import org.torproject.descriptor.RelayExtraInfoDescriptor;
import org.torproject.metrics.descriptorparser.utils.DateTimeHelper;
import org.torproject.metrics.descriptorparser.utils.DescriptorUtils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.sql.Array;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.Timestamp;
import java.util.Map;

public class ExtraInfoDescriptorParser {

  private boolean isBridge = true;

  private static final String INSERT_EXTRA_INFO_SQL
      = "INSERT INTO extra_info_descriptor"
      + " (is_bridge, published, nickname, fingerprint,"
      + " digest_sha1_hex, identity_ed25519,"
      + " master_key_ed25519, read_history,"
      + " write_history, ipv6_read_history,"
      + " ipv6_write_history, geoip_db_digest,"
      + " geoip6_db_digest, geoip_start_time,"
      + " geoip_client_origins, bridge_stats_end,"
      + " bridge_stats_inverval, bridge_ips,"
      + " bridge_ip_versions, bridge_ip_transports,"
      + " dirreq_stats_end, dirreq_stats_interval,"
      + " dirreq_v2_ips, dirreq_v3_ips,"
      + " dirreq_v2_reqs, dirreq_v3_reqs, dirreq_v2_share, dirreq_v3_share,"
      + " dirreq_v2_resp, dirreq_v3_resp,"
      + " dirreq_v2_direct_dl, dirreq_v3_direct_dl,"
      + " dirreq_v2_tunneled_dl, dirreq_v3_tunneled_dl,"
      + " dirreq_read_history, dirreq_write_history,"
      + " entry_stats_end, entry_stats_interval,"
      + " entry_ips, cell_stats_end,"
      + " cell_stats_interval, cell_processed_cells,"
      + " cell_queued_cells, cell_time_in_queue,"
      + " cell_circuits_per_decile, conn_bi_direct_timestamp,"
      + " conn_bi_direct_interval, conn_bi_direct_below,"
      + " conn_bi_direct_read, conn_bi_direct_write,"
      + " conn_bi_direct_both, ipv6_conn_bi_direct_timestamp,"
      + " ipv6_conn_bi_direct_interval, ipv6_conn_bi_direct_below,"
      + " ipv6_conn_bi_direct_read, ipv6_conn_bi_direct_write,"
      + " ipv6_conn_bi_direct_both, exit_stats_end,"
      + " exit_stats_inverval, exit_kibibytes_written,"
      + " exit_kibibytes_read, exit_streams_opened,"
      + " hidserv_stats_end, hidserv_stats_interval,"
      + " hidserv_v3_stats_end, hidserv_v3_stats_interval,"
      + " hidserv_rend_relayed_cells_value, hidserv_rend_relayed_cells,"
      + " hidserv_rend_v3_relayed_cells_value, hidserv_rend_v3_relayed_cells,"
      + " hidserv_dir_onions_seen_value, hidserv_dir_onions_seen,"
      + " hidserv_dir_v3_onions_seen_value, hidserv_dir_v3_onions_seen,"
      + " transports, padding_counts_timestamp,"
      + " padding_counts_interval, padding_counts, overload_ratelimits_version,"
      + " overload_ratelimits_timestamp,"
      + " overload_ratelimits_ratelimit, overload_ratelimits_burstlimit,"
      + " overload_ratelimits_readcount, overload_ratelimits_write_count,"
      + " overload_fd_exhausted_version, overload_fd_exhausted_timestamp,"
      + " router_sig_ed25519, router_signature, header) VALUES "
      + "(?, ?, ?, ?, ?, ?, ?, ?,"
      + " ?, ?, ?, ?, ?, ?, ?, ?,"
      + " ?, ?, ?, ?, ?, ?, ?, ?,"
      + " ?, ?, ?, ?, ?, ?, ?, ?,"
      + " ?, ?, ?, ?, ?, ?, ?, ?,"
      + " ?, ?, ?, ?, ?, ?, ?, ?,"
      + " ?, ?, ?, ?, ?, ?, ?, ?,"
      + " ?, ?, ?, ?, ?, ?, ?, ?,"
      + " ?, ?, ?, ?, ?, ?, ?, ?,"
      + " ?, ?, ?, ?, ?, ?, ?, ?,"
      + " ?, ?, ?, ?, ?, ?, ?, ?, ?"
      + ") ON CONFLICT DO NOTHING;";

  private static final Logger logger = LoggerFactory.getLogger(
      ExtraInfoDescriptorParser.class);

  private static final String INSERT_READ_BANDWIDTH_METRIC_SQL
      = "INSERT INTO read_bandwidth_metric"
      + "(time, nickname, fingerprint,"
      + " read_bandwidth, extra_info_descriptor_digest) VALUES "
      + "(?, ?, ?, ?, ?) ON CONFLICT DO NOTHING;";

  private static final String INSERT_WRITE_BANDWIDTH_METRIC_SQL
      = "INSERT INTO write_bandwidth_metric"
      + "(time, nickname, fingerprint,"
      + " write_bandwidth, extra_info_descriptor_digest) VALUES "
      + "(?, ?, ?, ?, ?) ON CONFLICT DO NOTHING;";

  private static final String INSERT_IPV6_READ_BANDWIDTH_METRIC_SQL
      = "INSERT INTO ipv6_read_bandwidth_metric"
      + "(time, nickname, fingerprint,"
      + " ipv6_read_bandwidth, extra_info_descriptor_digest) VALUES "
      + "(?, ?, ?, ?, ?) ON CONFLICT DO NOTHING;";

  private static final String INSERT_IPV6_WRITE_BANDWIDTH_METRIC_SQL
      = "INSERT INTO ipv6_write_bandwidth_metric"
      + "(time, nickname, fingerprint,"
      + " ipv6_write_bandwidth, extra_info_descriptor_digest) VALUES "
      + "(?, ?, ?, ?, ?) ON CONFLICT DO NOTHING;";

  private static final String INSERT_DIRREQ_READ_BANDWIDTH_METRIC_SQL
      = "INSERT INTO dirreq_read_bandwidth_metric"
      + "(time, nickname, fingerprint,"
      + " dirreq_read_bandwidth, extra_info_descriptor_digest) VALUES "
      + "(?, ?, ?, ?, ?) ON CONFLICT DO NOTHING;";

  private static final String INSERT_DIRREQ_WRITE_BANDWIDTH_METRIC_SQL
      = "INSERT INTO dirreq_write_bandwidth_metric"
      + "(time, nickname, fingerprint,"
      + " dirreq_write_bandwidth, extra_info_descriptor_digest) VALUES "
      + "(?, ?, ?, ?, ?) ON CONFLICT DO NOTHING;";

  /**
   * Parse extra info descriptors and add fields to the database.
   */
  public void run(String path, Connection conn) throws Exception {
    // Read descriptors from disk.
    DescriptorReader descriptorReader =
        DescriptorSourceFactory.createDescriptorReader();

    for (Descriptor descriptor :
        descriptorReader.readDescriptors(new File(path))) {
      if ((descriptor instanceof RelayExtraInfoDescriptor)
          | (descriptor instanceof BridgeExtraInfoDescriptor)) {
        ExtraInfoDescriptor desc;
        if (descriptor instanceof RelayExtraInfoDescriptor) {
          desc = (RelayExtraInfoDescriptor) descriptor;
          this.isBridge = false;
        } else {
          desc = (BridgeExtraInfoDescriptor) descriptor;
        }

        this.addDescriptor(desc, conn);

        if (desc.getReadHistory() != null) {
          BandwidthHistory readBandwidthHistory = desc.getReadHistory();
          this.addToMetrics(readBandwidthHistory,
              INSERT_READ_BANDWIDTH_METRIC_SQL, conn, desc);
        }
        if (desc.getWriteHistory() != null) {
          BandwidthHistory writeBandwidthHistory = desc.getWriteHistory();
          this.addToMetrics(writeBandwidthHistory,
              INSERT_WRITE_BANDWIDTH_METRIC_SQL, conn, desc);
        }
        if (desc.getIpv6ReadHistory() != null) {
          BandwidthHistory ipv6ReadBandwidthHistory = desc.getIpv6ReadHistory();
          this.addToMetrics(ipv6ReadBandwidthHistory,
              INSERT_IPV6_READ_BANDWIDTH_METRIC_SQL, conn, desc);
        }
        if (desc.getIpv6WriteHistory() != null) {
          BandwidthHistory ipv6WriteBandwidthHistory =
              desc.getIpv6WriteHistory();
          this.addToMetrics(ipv6WriteBandwidthHistory,
              INSERT_IPV6_WRITE_BANDWIDTH_METRIC_SQL, conn, desc);
        }
        if (desc.getDirreqReadHistory() != null) {
          BandwidthHistory dirreqReadBandwidthHistory =
              desc.getDirreqReadHistory();
          this.addToMetrics(dirreqReadBandwidthHistory,
              INSERT_DIRREQ_READ_BANDWIDTH_METRIC_SQL, conn, desc);
        }
        if (desc.getDirreqWriteHistory() != null) {
          BandwidthHistory dirreqWriteBandwidthHistory =
              desc.getDirreqWriteHistory();
          this.addToMetrics(dirreqWriteBandwidthHistory,
              INSERT_DIRREQ_WRITE_BANDWIDTH_METRIC_SQL, conn, desc);
        }

      } else {
        // We're only interested in extra-info descriptors
        continue;
      }
    }
  }

  private void addDescriptor(ExtraInfoDescriptor desc, Connection conn) {
    DescriptorUtils descUtils = new DescriptorUtils();
    try (
      PreparedStatement preparedStatement =
          conn.prepareStatement(INSERT_EXTRA_INFO_SQL);
    ) {
      preparedStatement.setBoolean(1, this.isBridge);
      preparedStatement.setTimestamp(2,
          new Timestamp(desc.getPublishedMillis()));
      preparedStatement.setString(3, desc.getNickname());
      preparedStatement.setString(4, desc.getFingerprint());
      preparedStatement.setString(5, desc.getDigestSha1Hex());
      preparedStatement.setString(6, desc.getIdentityEd25519());
      preparedStatement.setString(7, desc.getMasterKeyEd25519());
      preparedStatement.setString(8,
          descUtils.fieldAsString(desc.getReadHistory()));
      preparedStatement.setString(9,
          descUtils.fieldAsString(desc.getWriteHistory()));
      preparedStatement.setString(10,
          descUtils.fieldAsString(desc.getIpv6ReadHistory()));
      preparedStatement.setString(11,
          descUtils.fieldAsString(desc.getIpv6WriteHistory()));
      preparedStatement.setString(12,
          desc.getGeoipDbDigestSha1Hex());
      preparedStatement.setString(13,
          desc.getGeoip6DbDigestSha1Hex());
      preparedStatement.setTimestamp(14,
          new Timestamp(desc.getGeoipStartTimeMillis()));
      preparedStatement.setString(15,
          descUtils.fieldAsString(desc.getGeoipClientOrigins()));
      if (this.isBridge == true) {
        preparedStatement.setTimestamp(16,
            new Timestamp(desc.getBridgeStatsEndMillis()));
        preparedStatement.setLong(17,
            desc.getBridgeStatsIntervalLength());
        preparedStatement.setString(18,
            descUtils.fieldAsString(desc.getBridgeIps()));
        preparedStatement.setString(19,
            descUtils.fieldAsString(desc.getBridgeIpVersions()));
        preparedStatement.setString(20,
            descUtils.fieldAsString(desc.getBridgeIpTransports()));
      } else {
        preparedStatement.setTimestamp(16,
            new Timestamp(desc.getPublishedMillis()));
        preparedStatement.setLong(17, -1L);
        preparedStatement.setString(18, "");
        preparedStatement.setString(19, "");
        preparedStatement.setString(20, "");
      }
      preparedStatement.setTimestamp(21,
          new Timestamp(desc.getDirreqStatsEndMillis()));
      preparedStatement.setLong(22,
          desc.getDirreqStatsIntervalLength());
      preparedStatement.setString(23,
          descUtils.fieldAsString(desc.getDirreqV2Ips()));
      preparedStatement.setString(24,
          descUtils.fieldAsString(desc.getDirreqV3Ips()));
      preparedStatement.setString(25,
          descUtils.fieldAsString(desc.getDirreqV2Reqs()));
      preparedStatement.setString(26,
          descUtils.fieldAsString(desc.getDirreqV3Reqs()));
      preparedStatement.setDouble(27, desc.getDirreqV2Share());
      preparedStatement.setDouble(28, desc.getDirreqV3Share());
      preparedStatement.setString(29,
          descUtils.fieldAsString(desc.getDirreqV2Resp()));
      preparedStatement.setString(30,
          descUtils.fieldAsString(desc.getDirreqV3Resp()));
      preparedStatement.setString(31,
          descUtils.fieldAsString(desc.getDirreqV2DirectDl()));
      preparedStatement.setString(32,
          descUtils.fieldAsString(desc.getDirreqV3DirectDl()));
      preparedStatement.setString(33,
          descUtils.fieldAsString(desc.getDirreqV2TunneledDl()));
      preparedStatement.setString(34,
          descUtils.fieldAsString(desc.getDirreqV3TunneledDl()));
      preparedStatement.setString(35,
          descUtils.fieldAsString(desc.getDirreqReadHistory()));
      preparedStatement.setString(36,
          descUtils.fieldAsString(desc.getDirreqWriteHistory()));
      preparedStatement.setTimestamp(37,
          new Timestamp(desc.getEntryStatsEndMillis()));
      preparedStatement.setLong(38, desc.getEntryStatsIntervalLength());
      preparedStatement.setString(39,
          descUtils.fieldAsString(desc.getEntryIps()));
      preparedStatement.setTimestamp(40,
          new Timestamp(desc.getCellStatsEndMillis()));
      preparedStatement.setLong(41, desc.getCellStatsIntervalLength());
      Array arrayCellProcessedCells =
          conn.createArrayOf("int",
          descUtils.listToArray(desc.getCellProcessedCells()));
      preparedStatement.setArray(42, arrayCellProcessedCells);
      Array arrayCellQueuedCells =
          conn.createArrayOf("float8",
          descUtils.listToArray(desc.getCellQueuedCells()));
      preparedStatement.setArray(43, arrayCellQueuedCells);
      Array arrayCellTimeInQueue =
          conn.createArrayOf("float8",
          descUtils.listToArray(desc.getCellTimeInQueue()));
      preparedStatement.setArray(44, arrayCellTimeInQueue);
      preparedStatement.setLong(45, desc.getCellCircuitsPerDecile());
      preparedStatement.setTimestamp(46,
          new Timestamp(desc.getConnBiDirectStatsEndMillis()));
      preparedStatement.setLong(47, desc.getConnBiDirectStatsIntervalLength());
      preparedStatement.setLong(48, desc.getConnBiDirectBelow());
      preparedStatement.setLong(49, desc.getConnBiDirectRead());
      preparedStatement.setLong(50, desc.getConnBiDirectWrite());
      preparedStatement.setLong(51, desc.getConnBiDirectBoth());
      preparedStatement.setTimestamp(52,
          new Timestamp(desc.getIpv6ConnBiDirectStatsEndMillis()));
      preparedStatement.setLong(53,
          desc.getIpv6ConnBiDirectStatsIntervalLength());
      preparedStatement.setLong(54, desc.getIpv6ConnBiDirectBelow());
      preparedStatement.setLong(55, desc.getIpv6ConnBiDirectRead());
      preparedStatement.setLong(56, desc.getIpv6ConnBiDirectRead());
      preparedStatement.setLong(57, desc.getIpv6ConnBiDirectBoth());
      preparedStatement.setTimestamp(58,
          new Timestamp(desc.getExitStatsEndMillis()));
      preparedStatement.setLong(59, desc.getExitStatsIntervalLength());
      preparedStatement.setString(60,
          descUtils.fieldAsString(desc.getExitKibibytesWritten()));
      preparedStatement.setString(61,
          descUtils.fieldAsString(desc.getExitKibibytesRead()));
      preparedStatement.setString(62,
          descUtils.fieldAsString(desc.getExitStreamsOpened()));
      preparedStatement.setTimestamp(63,
          new Timestamp(desc.getHidservStatsEndMillis()));
      preparedStatement.setLong(64, desc.getHidservStatsIntervalLength());
      preparedStatement.setTimestamp(65,
          new Timestamp(desc.getHidservV3StatsEndMillis()));
      preparedStatement.setLong(66, desc.getHidservV3StatsIntervalLength());
      if (desc.getHidservRendRelayedCells() != null) {
        preparedStatement.setDouble(67, desc.getHidservRendRelayedCells());
      } else {
        preparedStatement.setDouble(67, Double.NaN);
      }
      preparedStatement.setString(68,
          descUtils.fieldAsString(
          desc.getHidservRendRelayedCellsParameters()));
      if (desc.getHidservRendV3RelayedCells() != null) {
        preparedStatement.setDouble(69, desc.getHidservRendV3RelayedCells());
      } else {
        preparedStatement.setDouble(69, Double.NaN);
      }
      preparedStatement.setString(70,
          descUtils.fieldAsString(
          desc.getHidservRendV3RelayedCellsParameters()));
      if (desc.getHidservDirOnionsSeen() != null ) {
        preparedStatement.setDouble(71, desc.getHidservDirOnionsSeen());
      } else {
        preparedStatement.setDouble(71, Double.NaN);
      }
      preparedStatement.setString(72,
          descUtils.fieldAsString(desc.getHidservDirOnionsSeenParameters()));
      if (desc.getHidservDirV3OnionsSeen() != null) {
        preparedStatement.setDouble(73, desc.getHidservDirV3OnionsSeen());
      } else {
        preparedStatement.setDouble(73, Double.NaN);
      }
      preparedStatement.setString(74,
          descUtils.fieldAsString(
          desc.getHidservDirV3OnionsSeenParameters()));
      preparedStatement.setString(75,
          descUtils.fieldAsString(desc.getTransports()));
      preparedStatement.setTimestamp(76,
          new Timestamp(desc.getPaddingCountsStatsEndMillis()));
      preparedStatement.setLong(77,
          desc.getPaddingCountsStatsIntervalLength());
      preparedStatement.setString(78,
          descUtils.fieldAsString(desc.getPaddingCounts()));
      preparedStatement.setInt(79, desc.getOverloadRatelimitsVersion());
      preparedStatement.setTimestamp(80,
          new Timestamp(desc.getOverloadRatelimitsTimestamp()));
      preparedStatement.setLong(81, desc.getOverloadRatelimitsRateLimit());
      preparedStatement.setLong(82, desc.getOverloadRatelimitsBurstLimit());
      preparedStatement.setLong(83, desc.getOverloadRatelimitsReadCount());
      preparedStatement.setLong(84, desc.getOverloadRatelimitsWriteCount());
      preparedStatement.setInt(85, desc.getOverloadFdExhaustedVersion());
      preparedStatement.setTimestamp(86,
          new Timestamp(desc.getOverloadFdExhaustedTimestamp()));
      preparedStatement.setString(87, desc.getRouterSignatureEd25519());
      preparedStatement.setString(88, desc.getRouterSignature());
      preparedStatement.setString(89, "@type extra-info 1.0");
      preparedStatement.executeUpdate();

    } catch (Exception ex) {
      ex.printStackTrace(System.out);
      logger.warn("Exception. {}".format(ex.getMessage()));
      ex.printStackTrace(System.out);
    }
  }

  private void addToMetrics(BandwidthHistory bandwidthHistory,
      String sqlFormattedStatement, Connection conn, ExtraInfoDescriptor desc) {
    DescriptorUtils descUtils = new DescriptorUtils();
    long intervalMillis = bandwidthHistory.getIntervalLength()
        * DateTimeHelper.ONE_SECOND;
    for (Map.Entry<Long, Long> e :
        bandwidthHistory.getBandwidthValues().entrySet()) {
      long endMillis = e.getKey();
      long startMillis = endMillis - intervalMillis;
      long bandwidthValue = e.getValue();
      try (
        PreparedStatement preparedStatement =
        conn.prepareStatement(sqlFormattedStatement);
      ) {
        preparedStatement.setTimestamp(1, new Timestamp(startMillis));
        preparedStatement.setString(2, desc.getNickname());
        preparedStatement.setString(3, desc.getFingerprint());
        preparedStatement.setLong(4, bandwidthValue);
        preparedStatement.setString(5, desc.getDigestSha1Hex());
        preparedStatement.executeUpdate();
      } catch (Exception ex) {
        logger.warn("Exception. {}".format(ex.getMessage()));
        ex.printStackTrace(System.out);
      }
    }
  }

}
