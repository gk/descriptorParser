package org.torproject.metrics.descriptorparser.parsers;

import org.torproject.descriptor.BandwidthFile;
import org.torproject.descriptor.Descriptor;
import org.torproject.descriptor.DescriptorReader;
import org.torproject.descriptor.DescriptorSourceFactory;

import org.torproject.metrics.descriptorparser.utils.DateTimeHelper;
import org.torproject.metrics.descriptorparser.utils.PsqlConnector;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.Timestamp;

public class BandwidthParser {
  private static final Logger logger = LoggerFactory.getLogger(
      BandwidthParser.class);

  private static final String INSERT_BANDWIDTH_FILE_SQL
      = "INSERT INTO"
      + " bandwidth_file (header, published, destination_countries,"
      + " earliest_bandwidth, file_created, generator_started,"
      + " latest_bandwidth, minimum_number_eligible_relays,"
      + " minimum_percent_eligible_relays, number_consensus_relays,"
      + " number_eligible_relays, percent_eligible_relays,"
      + " recent_consensus_count, recent_measurement_attempt_count,"
      + " recent_measurement_failure_count,"
      + " recent_measurements_excluded_error_count,"
      + " recent_measurements_excluded_few_count,"
      + " recent_measurements_excluded_near_count,"
      + " recent_measurements_excluded_old_count,"
      + " recent_priority_list_count, recent_priority_relay_count,"
      + " scanner_country, software, software_version,"
      + " time_to_report_half_network, tor_version, digest)"
      + " VALUES "
      + "(?, ?, ?, ?, ?, ?, ?, ?, ?, ?,"
      + " ?, ?, ?, ?, ?, ?, ?, ?, ?, ?,"
      + " ?, ?, ?, ?, ?, ?, ?)";

  private static final String INSERT_BANDWIDTH_RECORD_SQL
      = "INSERT INTO"
      + " bandwidth_record (bw, bw_mean, bw_median, consensus_bandwidth,"
      + " consensus_bandwidth_is_unmeasured, desc_bw_avg, desc_bw_bur,"
      + " desc_bw_obs_last, desc_bw_obs_mean, error_circ, error_destination,"
      + " error_misc, error_second_relay,"
      + " error_stream, master_key_ed25519, nick,"
      + " node_id, relay_in_recent_consensus_count,"
      + " relay_recent_measurement_attempt_count,"
      + " relay_recent_measurements_excluded_error_count,"
      + " relay_recent_priority_list_count, success,"
      + " time, bandwidth_file)"
      + " VALUES "
      + "(?, ?, ?, ?, ?, ?, ?, ?, ?, ?,"
      + " ?, ?, ?, ?, ?, ?, ?, ?, ?, ?,"
      + " ?, ?, ?, ?)";

  /**
   * Parse badnwidth files and add fields to the database.
   */
  public void run(String path, Connection conn) throws Exception {
    PsqlConnector psqlConn = new PsqlConnector();
    // Read descriptors from disk.
    DescriptorReader descriptorReader =
        DescriptorSourceFactory.createDescriptorReader();

    for (Descriptor descriptor : descriptorReader.readDescriptors(
        new File(path))) {
      if (descriptor instanceof BandwidthFile) {
        BandwidthFile desc = (BandwidthFile) descriptor;

        this.addBandwidthFile(desc, conn);
        for (BandwidthFile.RelayLine relayLine : desc.relayLines()) {
          String digest = desc.digestSha256Base64();
          this.addRelayLine(relayLine, digest, conn);
        }

      } else {
        continue;
      }
    }
  }

  private void addRelayLine(BandwidthFile.RelayLine relayLine, String digest,
      Connection conn) {
    try (
      PreparedStatement preparedStatement = conn.prepareStatement(
          INSERT_BANDWIDTH_RECORD_SQL);
    ) {
      preparedStatement.setLong(1, relayLine.bw());
      if (relayLine.additionalKeyValues().get("bw_mean") != null) {
        long bwMean = Long.parseLong(
            relayLine.additionalKeyValues().get("bw_mean"));
        preparedStatement.setLong(2, bwMean);
      } else {
        preparedStatement.setLong(2, -1L);
      }
      if (relayLine.additionalKeyValues().get("bw_median") != null) {
        long bwMedian = Long.parseLong(
            relayLine.additionalKeyValues().get("bw_median"));
        preparedStatement.setLong(3, bwMedian);
      } else {
        preparedStatement.setLong(3, -1L);
      }
      if (relayLine.additionalKeyValues().get("consensus_bandwidth") != null) {
        long consensusBw = Long.parseLong(
            relayLine.additionalKeyValues().get("consensus_bandwidth"));
        preparedStatement.setLong(4, consensusBw);
      } else {
        preparedStatement.setLong(4, -1L);
      }
      boolean consensusBwUnmeasured =
          Boolean.parseBoolean(
          relayLine.additionalKeyValues()
          .get("consensus_bandwidth_is_unmeasured"));
      preparedStatement.setBoolean(5, consensusBwUnmeasured);
      if (relayLine.additionalKeyValues().get("desc_bw_avg") != null) {
        long descBwAvg = Long.parseLong(
            relayLine.additionalKeyValues().get("desc_bw_avg"));
        preparedStatement.setLong(6, descBwAvg);
      } else {
        preparedStatement.setLong(6, -1L);
      }
      if (relayLine.additionalKeyValues().get("desc_bw_bur") != null) {
        long descBwBur = Long.parseLong(
            relayLine.additionalKeyValues().get("desc_bw_bur"));
        preparedStatement.setLong(7, descBwBur);
      } else {
        preparedStatement.setLong(7, -1L);
      }
      if (relayLine.additionalKeyValues().get("desc_bw_obs_last") != null) {
        long descBwObsLast = Long.parseLong(
            relayLine.additionalKeyValues().get("desc_bw_obs_last"));
        preparedStatement.setLong(8, descBwObsLast);
      } else {
        preparedStatement.setLong(8, -1L);
      }
      if (relayLine.additionalKeyValues().get("desc_bw_obs_mean") != null) {
        long descBwObsMean = Long.parseLong(
            relayLine.additionalKeyValues().get("desc_bw_obs_mean"));
        preparedStatement.setLong(9, descBwObsMean);
      } else {
        preparedStatement.setLong(9, -1L);
      }
      int errorCirc = Integer.parseInt(
          relayLine.additionalKeyValues().get("error_circ"));
      preparedStatement.setInt(10, errorCirc);
      int errorDestination = Integer.parseInt(
          relayLine.additionalKeyValues().get("error_destination"));
      preparedStatement.setInt(11, errorDestination);
      int errorMisc = Integer.parseInt(
          relayLine.additionalKeyValues().get("error_misc"));
      preparedStatement.setInt(12, errorMisc);
      int errorNdRelay = Integer.parseInt(
          relayLine.additionalKeyValues().get("error_second_relay"));
      preparedStatement.setInt(13, errorNdRelay);
      int errorStream = Integer.parseInt(
          relayLine.additionalKeyValues().get("error_stream"));
      preparedStatement.setInt(14, errorStream);
      String masterKey = "";
      if (relayLine.masterKeyEd25519().isPresent()) {
        masterKey = relayLine.masterKeyEd25519().get();
      }
      preparedStatement.setString(15, masterKey);
      String nick = relayLine.additionalKeyValues().get("nick");
      preparedStatement.setString(16, nick);
      preparedStatement.setString(17, relayLine.nodeId().get());
      int relayConsensusCount = Integer.parseInt(
          relayLine.additionalKeyValues()
          .get("relay_in_recent_consensus_count"));
      preparedStatement.setInt(18, relayConsensusCount);
      int relayAttemptCount = Integer.parseInt(
          relayLine.additionalKeyValues()
          .get("relay_recent_measurement_attempt_count"));
      preparedStatement.setInt(19, relayAttemptCount);
      int relayExcludedCount = 0;
      if (relayLine.additionalKeyValues()
          .get("relay_recent_measurements_excluded_error_count") != null) {
        relayExcludedCount = Integer.parseInt(
            relayLine.additionalKeyValues()
            .get("relay_recent_measurements_excluded_error_count"));
      }
      preparedStatement.setInt(20, relayExcludedCount);
      int relayPriorityCount = Integer.parseInt(
          relayLine.additionalKeyValues()
          .get("relay_recent_priority_list_count"));
      preparedStatement.setInt(21, relayPriorityCount);
      int success = Integer.parseInt(
          relayLine.additionalKeyValues().get("success"));
      preparedStatement.setInt(22, success);
      String relayTime = relayLine.additionalKeyValues().get("time");
      String dateFormat = "yyyy-MM-dd'T'HH:mm:ss";
      long rt = DateTimeHelper.parse(relayTime, dateFormat);
      preparedStatement.setTimestamp(23, new Timestamp(rt));
      preparedStatement.setString(24, digest);
      preparedStatement.executeUpdate();
    } catch (Exception ex) {
      logger.warn("Exception. {}".format(ex.getMessage()));
    }
  }

  private void addBandwidthFile(BandwidthFile desc, Connection conn) {
    try (
      PreparedStatement preparedStatement = conn.prepareStatement(
          INSERT_BANDWIDTH_FILE_SQL);
    ) {
      preparedStatement.setString(1, "@type bandwidth-file 1.0");
      Timestamp timestamp = Timestamp.valueOf(desc.timestamp());
      preparedStatement.setTimestamp(2, timestamp);
      String destinationsCountries =
          String.join(", ", desc.destinationsCountries().get());
      preparedStatement.setString(3, destinationsCountries);
      Timestamp earliestBandwidth =
          Timestamp.valueOf(desc.earliestBandwidth().get());
      preparedStatement.setTimestamp(4, earliestBandwidth);
      Timestamp fileCreated =
          Timestamp.valueOf(desc.fileCreated().get());
      preparedStatement.setTimestamp(5, fileCreated);
      Timestamp generatorStarted =
          Timestamp.valueOf(desc.generatorStarted().get());
      preparedStatement.setTimestamp(6, generatorStarted);
      Timestamp latestBandwidth =
          Timestamp.valueOf(desc.latestBandwidth().get());
      preparedStatement.setTimestamp(7, latestBandwidth);
      preparedStatement.setInt(8, desc.minimumNumberEligibleRelays().get());
      preparedStatement.setInt(9,
          desc.minimumPercentEligibleRelays().get());
      preparedStatement.setInt(10, desc.numberConsensusRelays().get());
      preparedStatement.setInt(11, desc.numberEligibleRelays().get());
      preparedStatement.setInt(12, desc.percentEligibleRelays().get());
      preparedStatement.setInt(13, desc.recentConsensusCount().get());
      if (desc.recentMeasurementAttemptCount().isPresent()) {
        preparedStatement.setInt(14,
            desc.recentMeasurementAttemptCount().get());
      } else {
        preparedStatement.setInt(14, 0);
      }
      if (desc.recentMeasurementFailureCount().isPresent()) {
        preparedStatement.setInt(15,
            desc.recentMeasurementFailureCount().get());
      } else {
        preparedStatement.setInt(15, 0);
      }
      preparedStatement.setInt(16,
          desc.recentMeasurementsExcludedErrorCount().get());
      preparedStatement.setInt(17,
          desc.recentMeasurementsExcludedFewCount().get());
      preparedStatement.setInt(18,
          desc.recentMeasurementsExcludedNearCount().get());
      preparedStatement.setInt(19,
          desc.recentMeasurementsExcludedOldCount().get());
      preparedStatement.setInt(20,
          desc.recentPriorityListCount().get());
      preparedStatement.setInt(21,
          desc.recentPriorityRelayCount().get());
      preparedStatement.setString(22, desc.scannerCountry().get());
      preparedStatement.setString(23, desc.software());
      preparedStatement.setString(24, desc.softwareVersion().get());
      preparedStatement.setLong(25,
          desc.timeToReportHalfNetwork().get().getSeconds());
      preparedStatement.setString(26, desc.version());
      preparedStatement.setString(27, desc.digestSha256Base64());
      preparedStatement.executeUpdate();
    } catch (Exception ex) {
      logger.warn("Exception. {}".format(ex.getMessage()));
    }
  }
}
