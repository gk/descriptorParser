package org.torproject.metrics.descriptorparser.parsers;

import org.torproject.descriptor.BridgeNetworkStatus;
import org.torproject.descriptor.Descriptor;
import org.torproject.descriptor.DescriptorReader;
import org.torproject.descriptor.DescriptorSourceFactory;
import org.torproject.descriptor.NetworkStatusEntry;
import org.torproject.metrics.descriptorparser.utils.DescriptorUtils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.sql.Connection;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.Timestamp;
import java.util.Map;

public class BridgeNetworkStatusParser {
  private static final String INSERT_BRIDGE_NETWORK_STATUS_SQL
      = "INSERT INTO"
      + " bridge_network_status (published, fingerprint, flag_thresholds,"
      + " stable_uptime, stable_mtbf, fast_bandwidth, guard_wfu, guard_tk,"
      + " guard_bandwidth_including_exits, guard_bandwidth_excluding_exits,"
      + " enough_mtbf_info, ignore_adv_bws, header, digest) VALUES "
      + "(?, ?, ?, ?, ?, ?, ?,"
      + " ?, ?, ?, ?, ?, ?, ?) ON CONFLICT DO NOTHING;";

  private static final String INSERT_BRIDGE_STATUS_SQL
      = "INSERT INTO"
      + " bridge_status (published, fingerprint, nickname, digest,"
      + " network_status, address, or_port, dir_port, or_address,"
      + " flags, bandwidth, policy) VALUES "
      + "(?, ?, ?, ?, ?, ?,"
      + " ?, ?, ?, ?, ?, ?) ON CONFLICT DO NOTHING;";

  private static final Logger logger = LoggerFactory.getLogger(
      BridgeNetworkStatusParser.class);

  /**
   * Parse bridge network statuses and add fields to the database.
   */
  public void run(String path, Connection conn) throws Exception {
    DescriptorUtils descUtils = new DescriptorUtils();
    // Read descriptors from disk.
    DescriptorReader descriptorReader =
        DescriptorSourceFactory.createDescriptorReader();

    for (Descriptor descriptor : descriptorReader.readDescriptors(
        new File(path))) {
      if (descriptor instanceof BridgeNetworkStatus) {
        BridgeNetworkStatus desc = (BridgeNetworkStatus) descriptor;
        String digest = descUtils.calculateDigestSha256Base64(
            desc.getRawDescriptorBytes());

        this.addNetworkStatus(desc, digest, conn);
        for (Map.Entry<String, NetworkStatusEntry> e :
            desc.getStatusEntries().entrySet()) {
          String fingerprint = e.getKey();
          NetworkStatusEntry entry = e.getValue();

          this.addBridgeStatus(fingerprint, entry,
              digest, conn);
        }

      } else {
        // We're only interested in bridge-network-satuses
        continue;
      }
    }
  }

  private void addBridgeStatus(String fingerprint, NetworkStatusEntry entry,
      String networkStatusDigest, Connection conn) {
    DescriptorUtils descUtils = new DescriptorUtils();
    try (
      PreparedStatement preparedStatement =
          conn.prepareStatement(INSERT_BRIDGE_STATUS_SQL);
    ) {
      preparedStatement.setTimestamp(1,
          new Timestamp(entry.getPublishedMillis()));
      preparedStatement.setString(2, entry.getFingerprint());
      preparedStatement.setString(3, entry.getNickname());
      preparedStatement.setString(4, descUtils.calculateDigestSha256Base64(
          entry.getStatusEntryBytes()));
      preparedStatement.setString(5, networkStatusDigest);
      preparedStatement.setString(6, entry.getAddress());
      preparedStatement.setInt(7, entry.getOrPort());
      preparedStatement.setInt(8, entry.getDirPort());
      preparedStatement.setString(9, descUtils.fieldAsString(
          entry.getOrAddresses()));
      preparedStatement.setString(10, descUtils.fieldAsString(
          entry.getFlags()));
      preparedStatement.setLong(11, entry.getBandwidth());
      preparedStatement.setString(12, entry.getDefaultPolicy());
      preparedStatement.executeUpdate();
    } catch (Exception ex) {
      logger.warn("Exception. {}".format(ex.getMessage()));
    }
  }

  private void addNetworkStatus(BridgeNetworkStatus desc, String digest,
        Connection conn) {
    try (
      PreparedStatement preparedStatement = conn.prepareStatement(
          INSERT_BRIDGE_NETWORK_STATUS_SQL);
    ) {
      preparedStatement.setTimestamp(1,
          new Timestamp(desc.getPublishedMillis()));
      preparedStatement.setString(2, desc.getFingerprint());
      String flagThresholds = String.format("stable-uptime=%s stable-mtbf=%s"
          + " fast-speed=%s"
          + " guard-wfu=%s guard-tk=%s guard-bw-inc-exits=%s"
          + " guard-bw-exc-exits=%s enough-mtbf=%s"
          + " ignoring-advertised-bws=%s",
          desc.getStableUptime(),
          desc.getStableMtbf(),
          desc.getFastBandwidth(),
          desc.getGuardWfu(),
          desc.getGuardTk(),
          desc.getGuardBandwidthIncludingExits(),
          desc.getGuardBandwidthExcludingExits(),
          desc.getEnoughMtbfInfo(),
          desc.getIgnoringAdvertisedBws());
      preparedStatement.setString(3, flagThresholds);
      preparedStatement.setLong(4, desc.getStableUptime());
      preparedStatement.setLong(5, desc.getStableMtbf());
      preparedStatement.setLong(6, desc.getFastBandwidth());
      preparedStatement.setDouble(7, desc.getGuardWfu());
      preparedStatement.setLong(8, desc.getGuardTk());
      preparedStatement.setLong(9, desc.getGuardBandwidthIncludingExits());
      preparedStatement.setLong(10, desc.getGuardBandwidthExcludingExits());
      preparedStatement.setInt(11, desc.getEnoughMtbfInfo());
      preparedStatement.setInt(12, desc.getIgnoringAdvertisedBws());
      /*
       * We can hardcode the header for now, but it would be better to add
       * the parsing logic to metrics lib in case this change
       **/
      preparedStatement.setString(13, "@type bridge-network-status 1.2");
      preparedStatement.setString(14, digest);
      preparedStatement.executeUpdate();
    } catch (Exception ex) {
      logger.warn("Exception. {}".format(ex.getMessage()));
    }
  }
}
