package org.torproject.metrics.descriptorparser.parsers;

import org.torproject.descriptor.Descriptor;
import org.torproject.descriptor.DescriptorReader;
import org.torproject.descriptor.DescriptorSourceFactory;
import org.torproject.descriptor.NetworkStatusEntry;
import org.torproject.descriptor.RelayNetworkStatusConsensus;
import org.torproject.metrics.descriptorparser.utils.DescriptorUtils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.Timestamp;
import java.util.Map;

public class ConsensusParser {
  private static final Logger logger = LoggerFactory.getLogger(
      ConsensusParser.class);

  private static final String INSERT_NETWORK_STATUS_SQL
      = "INSERT INTO"
      + " network_status (header, network_status_version,"
      + " consensus_method, consensus_flavor, valid_after, fresh_until,"
      + " valid_until, vote_seconds, dist_seconds, known_flags,"
      + " recommended_client_version, recommended_server_version,"
      + " recommended_client_protocols, recommended_relay_protocols,"
      + " required_client_protocols, required_relay_protocols,"
      + " params, package_lines, shared_rand_previous_value,"
      + " shared_rand_current_value,"
      + " shared_rand_previous_num, shared_rand_current_num, dir_sources,"
      + " digest, bandwidth_weights, directory_signatures) VALUES "
      + "(?, ?, ?, ?, ?, ?, ?,"
      + " ?, ?, ?, ?, ?, ?, ?,"
      + " ?, ?, ?, ?, ?, ?, ?,"
      + " ?, ?, ?, ?, ?)";

  private static final String INSERT_NETWORK_STATUS_ENTRY_SQL
      = "INSERT INTO network_status_entry"
      + " (nickname, fingerprint, digest, time, ip, or_port,"
      + " dir_port, or_addresses, flags, version, bandwidth_measured,"
      + " bandwidth_unmesured, bandwidth_weight, proto, policy,"
      + " port_list, master_key_ed25519, network_status) VALUES "
      + "(?, ?, ?, ?, ?, ?, ?,"
      + " ?, ?, ?, ?, ?, ?, ?,"
      + " ?, ?, ?, ?)";

  /**
   * Parse consensus files and add fields to the database.
   */
  public void run(String path, Connection conn) throws Exception {
    DescriptorUtils descUtils = new DescriptorUtils();
    // Read descriptors from disk.
    DescriptorReader descriptorReader =
        DescriptorSourceFactory.createDescriptorReader();

    for (Descriptor descriptor : descriptorReader.readDescriptors(
        new File(path))) {
      if (descriptor instanceof RelayNetworkStatusConsensus) {
        RelayNetworkStatusConsensus desc =
            (RelayNetworkStatusConsensus) descriptor;

        String digest = descUtils.calculateDigestSha256Base64(
            desc.getRawDescriptorBytes());
        this.addNetworkStatus(desc, digest, conn);

        for (Map.Entry<String, NetworkStatusEntry> e :
            desc.getStatusEntries().entrySet()) {
          String fingerprint = e.getKey();
          NetworkStatusEntry entry = e.getValue();

          this.addRelayStatus(fingerprint, entry,
              digest, conn);
        }
      } else {
        // We're only interested in bridge-network-satuses
        continue;
      }
    }

  }

  private void addRelayStatus(String fingerprint, NetworkStatusEntry entry,
      String digest, Connection conn) {
    DescriptorUtils descUtils = new DescriptorUtils();
    try (
      PreparedStatement preparedStatement = conn.prepareStatement(
          INSERT_NETWORK_STATUS_ENTRY_SQL);
    ) {
      preparedStatement.setString(1, entry.getNickname());
      preparedStatement.setString(2, entry.getFingerprint());
      String entryDigest = descUtils.calculateDigestSha256Base64(
          entry.getStatusEntryBytes());
      preparedStatement.setString(3, entryDigest);
      preparedStatement.setTimestamp(4,
          new Timestamp(entry.getPublishedMillis()));
      preparedStatement.setString(5, entry.getAddress());
      preparedStatement.setInt(6, entry.getOrPort());
      preparedStatement.setInt(7, entry.getDirPort());
      preparedStatement.setString(8,
          descUtils.fieldAsString(entry.getOrAddresses()));
      preparedStatement.setString(9,
          descUtils.fieldAsString(entry.getFlags()));
      preparedStatement.setString(10, entry.getVersion());
      preparedStatement.setLong(11, entry.getMeasured());
      preparedStatement.setBoolean(12, entry.getUnmeasured());
      preparedStatement.setLong(13, entry.getBandwidth());
      preparedStatement.setString(14,
          descUtils.fieldAsString(entry.getProtocols()));
      preparedStatement.setString(15, entry.getDefaultPolicy());
      preparedStatement.setString(16, entry.getPortList());
      preparedStatement.setString(17, entry.getMasterKeyEd25519());
      preparedStatement.setString(18, digest);
      preparedStatement.executeUpdate();
    } catch (Exception ex) {
      logger.warn("Exception. {}".format(ex.getMessage()));
    }
  }

  private void addNetworkStatus(RelayNetworkStatusConsensus desc, String digest,
      Connection conn) {
    DescriptorUtils descUtils = new DescriptorUtils();
    try (
      PreparedStatement preparedStatement = conn.prepareStatement(
          INSERT_NETWORK_STATUS_SQL);
    ) {
      preparedStatement.setString(1, "@type network-status-consensus-3 1.0");
      preparedStatement.setInt(2, desc.getNetworkStatusVersion());
      preparedStatement.setInt(3, desc.getConsensusMethod());
      if (desc.getConsensusFlavor() != null) {
        preparedStatement.setString(4, desc.getConsensusFlavor());
      } else {
        preparedStatement.setString(4, "unflavored");
      }
      preparedStatement.setTimestamp(5,
          new Timestamp(desc.getValidAfterMillis()));
      preparedStatement.setTimestamp(6,
          new Timestamp(desc.getFreshUntilMillis()));
      preparedStatement.setTimestamp(7,
          new Timestamp(desc.getValidUntilMillis()));
      preparedStatement.setLong(8, desc.getVoteSeconds());
      preparedStatement.setLong(9, desc.getDistSeconds());
      preparedStatement.setString(10,
          descUtils.fieldAsString(desc.getKnownFlags()));
      preparedStatement.setString(11,
          String.join(", ", desc.getRecommendedClientVersions()));
      preparedStatement.setString(12,
          String.join(", ", desc.getRecommendedServerVersions()));
      preparedStatement.setString(13,
          descUtils.fieldAsString(desc.getRecommendedClientProtocols()));
      preparedStatement.setString(14,
          descUtils.fieldAsString(desc.getRecommendedRelayProtocols()));
      preparedStatement.setString(15,
          descUtils.fieldAsString(desc.getRequiredClientProtocols()));
      preparedStatement.setString(16,
          descUtils.fieldAsString(desc.getRequiredRelayProtocols()));
      preparedStatement.setString(17,
          descUtils.fieldAsString(desc.getConsensusParams()));
      preparedStatement.setString(18,
          descUtils.fieldAsString(desc.getPackageLines()));
      preparedStatement.setString(19,
          desc.getSharedRandPreviousValue());
      preparedStatement.setString(20,
          desc.getSharedRandCurrentValue());
      preparedStatement.setInt(21,
          desc.getSharedRandPreviousNumReveals());
      preparedStatement.setInt(22,
          desc.getSharedRandCurrentNumReveals());
      preparedStatement.setString(23,
          descUtils.fieldAsString(desc.getDirSourceEntries()));
      preparedStatement.setString(24, digest);
      preparedStatement.setString(25,
          descUtils.fieldAsString(desc.getBandwidthWeights()));
      preparedStatement.setString(26,
          descUtils.fieldAsString(desc.getSignatures()));
      preparedStatement.executeUpdate();
    } catch (Exception ex) {
      logger.warn("Exception. {}".format(ex.getMessage()));
    }
  }
}
