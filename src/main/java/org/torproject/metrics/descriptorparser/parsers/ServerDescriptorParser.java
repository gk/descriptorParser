package org.torproject.metrics.descriptorparser.parsers;

import org.torproject.descriptor.BridgeServerDescriptor;
import org.torproject.descriptor.Descriptor;
import org.torproject.descriptor.DescriptorReader;
import org.torproject.descriptor.DescriptorSourceFactory;
import org.torproject.descriptor.RelayServerDescriptor;
import org.torproject.descriptor.ServerDescriptor;
import org.torproject.metrics.descriptorparser.utils.DescriptorUtils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.sql.Array;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.Timestamp;

public class ServerDescriptorParser {
  private boolean isBridge = true;

  private static final String INSERT_SERVER_SQL
      = "INSERT INTO server_descriptor"
      + " (is_bridge, published, nickname, fingerprint,"
      + " digest_sha1_hex, identity_ed25519, master_key_ed25519, addresses,"
      + " or_port, socks_port, dir_port, or_addresses,"
      + " bandwidth_rate, bandwidth_burst, bandwidth_observed, platform,"
      + " overload_general_timestamp, overload_general_version,"
      + " protocols, is_hibernating,"
      + " uptime, onion_key, signing_key, exit_policy,"
      + " contacts, bridge_distribution_request, family, read_history,"
      + " write_history, uses_enhanced_dns_logic,"
      + " caches_extra_info, extra_info_sha1_digest,"
      + " hidden_service_dir_version, is_hidden_service_dir,"
      + " link_protocol_version, circuit_protocol_version,"
      + " allow_single_hop_exits, ipv6_default_policy,"
      + " ipv6_port_list, ntor_onion_key,"
      + " onion_key_cross_cert, ntor_onion_key_cross_cert,"
      + " ntor_onion_key_cross_cert_sign, tunnelled_dir_server,"
      + " router_sig_ed25519, router_signature, header) VALUES"
      + "(?, ?, ?, ?, ?, ?, ?, ?,"
      + " ?, ?, ?, ?, ?, ?, ?, ?,"
      + " ?, ?, ?, ?, ?, ?, ?, ?,"
      + " ?, ?, ?, ?, ?, ?, ?, ?,"
      + " ?, ?, ?, ?, ?, ?, ?, ?,"
      + " ?, ?, ?, ?, ?, ?, ?) ON CONFLICT DO NOTHING;";

  private static final Logger logger = LoggerFactory.getLogger(
      ServerDescriptorParser.class);

  private static final String INSERT_BANDWIDTH_METRICS_SQL
      = "INSERT INTO server_bandwidth_metrics"
      + " (time, nickname, fingerprint, server_descriptor_digest,"
      + " bandwidth_rate, bandwidth_burst, bandwidth_observed) VALUES "
      + "(?, ?, ?, ?, ?, ?, ?) ON CONFLICT DO NOTHING;";

  /**
   * Parse server descriptors and add fields to the database.
   */
  public void run(String path, Connection conn) throws Exception {
    // Read descriptors from disk.
    DescriptorReader descriptorReader =
        DescriptorSourceFactory.createDescriptorReader();

    for (Descriptor descriptor :
        descriptorReader.readDescriptors(new File(path))) {
      if ((descriptor instanceof RelayServerDescriptor)
          | (descriptor instanceof BridgeServerDescriptor)) {
        ServerDescriptor desc;
        if (descriptor instanceof RelayServerDescriptor) {
          desc = (RelayServerDescriptor) descriptor;
          this.isBridge = false;
        } else {
          desc = (BridgeServerDescriptor) descriptor;
        }

        this.addDescriptor(desc, conn);
        this.addToMetrics(INSERT_BANDWIDTH_METRICS_SQL, conn, desc);

      } else {
        // We're only interested in Server descriptors
        continue;
      }
    }
  }

  private void addDescriptor(ServerDescriptor desc, Connection conn) {
    DescriptorUtils descUtils = new DescriptorUtils();
    try (
      PreparedStatement preparedStatement =
          conn.prepareStatement(INSERT_SERVER_SQL);
    ) {
      preparedStatement.setBoolean(1, this.isBridge);
      preparedStatement.setTimestamp(2,
          new Timestamp(desc.getPublishedMillis()));
      preparedStatement.setString(3, desc.getNickname());
      preparedStatement.setString(4, desc.getFingerprint());
      preparedStatement.setString(5, desc.getDigestSha1Hex());
      preparedStatement.setString(6, desc.getIdentityEd25519());
      preparedStatement.setString(7, desc.getMasterKeyEd25519());
      preparedStatement.setString(8,
          descUtils.fieldAsString(desc.getAddress()));
      preparedStatement.setString(9,
          descUtils.fieldAsString(desc.getOrPort()));
      preparedStatement.setString(10,
          descUtils.fieldAsString(desc.getSocksPort()));
      preparedStatement.setString(11,
          descUtils.fieldAsString(desc.getDirPort()));
      preparedStatement.setString(12,
          descUtils.fieldAsString(desc.getOrAddresses()));
      preparedStatement.setLong(13, desc.getBandwidthRate());
      preparedStatement.setLong(14, desc.getBandwidthBurst());
      preparedStatement.setLong(15, desc.getBandwidthObserved());
      preparedStatement.setString(16, desc.getPlatform());
      preparedStatement.setLong(17, desc.getOverloadGeneralTimestamp());
      if (desc.getOverloadGeneralVersion() != -1L) {
        preparedStatement.setInt(18, (int)desc.getOverloadGeneralVersion());
      } else {
        preparedStatement.setInt(18, -1);
      }
      preparedStatement.setString(19,
          descUtils.fieldAsString(desc.getProtocols()));
      preparedStatement.setBoolean(20, desc.isHibernating());
      preparedStatement.setLong(21, desc.getUptime());
      preparedStatement.setString(22, desc.getOnionKey());
      preparedStatement.setString(23, desc.getSigningKey());
      preparedStatement.setString(24,
          descUtils.fieldAsString(desc.getExitPolicyLines()));
      preparedStatement.setString(25, desc.getContact());
      if (this.isBridge == true) {
        preparedStatement.setString(26, desc.getBridgeDistributionRequest());
      } else {
        preparedStatement.setString(26, "");
      }
      preparedStatement.setString(27,
          descUtils.fieldAsString(desc.getFamilyEntries()));
      preparedStatement.setString(28,
          descUtils.fieldAsString(desc.getReadHistory()));
      preparedStatement.setString(29,
          descUtils.fieldAsString(desc.getWriteHistory()));
      preparedStatement.setBoolean(30,
          desc.getUsesEnhancedDnsLogic());
      preparedStatement.setBoolean(31, desc.getCachesExtraInfo());
      preparedStatement.setString(32, desc.getExtraInfoDigestSha1Hex());
      preparedStatement.setString(33, "");
      preparedStatement.setBoolean(34, desc.isHiddenServiceDir());
      Array arrayLinkProtocolVersions =
          conn.createArrayOf("int",
          descUtils.listToArray(desc.getLinkProtocolVersions()));
      preparedStatement.setArray(35, arrayLinkProtocolVersions);
      Array arrayCircuitProtocolVersions =
          conn.createArrayOf("int",
          descUtils.listToArray(desc.getCircuitProtocolVersions()));
      preparedStatement.setArray(36, arrayCircuitProtocolVersions);
      preparedStatement.setBoolean(37, desc.getAllowSingleHopExits());
      preparedStatement.setString(38,
          descUtils.fieldAsString(desc.getIpv6DefaultPolicy()));
      preparedStatement.setString(39,
          descUtils.fieldAsString(desc.getIpv6PortList()));
      preparedStatement.setString(40, desc.getNtorOnionKey());
      preparedStatement.setString(41, desc.getOnionKeyCrosscert());
      preparedStatement.setString(42, desc.getNtorOnionKeyCrosscert());
      preparedStatement.setInt(43, desc.getNtorOnionKeyCrosscertSign());
      preparedStatement.setBoolean(44, desc.getTunnelledDirServer());
      preparedStatement.setString(45, desc.getRouterSignatureEd25519());
      preparedStatement.setString(46, desc.getRouterSignature());
      preparedStatement.setString(47, "@type server-descriptor 1.0");
      preparedStatement.executeUpdate();
    } catch (Exception ex) {
      ex.printStackTrace(System.out);
      System.out.println(ex);
      logger.warn("Exception. {}".format(ex.getMessage()));
    }

  }

  private void addToMetrics(String sqlFormattedStatement,
      Connection conn, ServerDescriptor desc) {
    try (
      PreparedStatement preparedStatement =
          conn.prepareStatement(sqlFormattedStatement);
    ) {
      preparedStatement.setTimestamp(1,
          new Timestamp(desc.getPublishedMillis()));
      preparedStatement.setString(2, desc.getNickname());
      preparedStatement.setString(3, desc.getFingerprint());
      preparedStatement.setString(4, desc.getDigestSha1Hex());
      preparedStatement.setLong(5, desc.getBandwidthRate());
      preparedStatement.setLong(6, desc.getBandwidthBurst());
      preparedStatement.setLong(7, desc.getBandwidthObserved());
      preparedStatement.executeUpdate();
    }
    catch (Exception ex) {
      logger.warn("Exception. {}".format(ex.getMessage()));
      ex.printStackTrace(System.out);
    }
  }
}
