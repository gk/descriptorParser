package org.torproject.metrics.descriptorparser.parsers;

import static junit.framework.TestCase.assertEquals;

import org.torproject.metrics.descriptorparser.utils.PsqlConnector;

import org.junit.Test;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

public class ConsensusParserTest {

  @Test()
  public void testConsensusParserDbUploader() throws Exception {
    ConsensusParser cp = new ConsensusParser();
    String consensusPath =
        "src/test/resources/2016-09-20-13-00-00-consensus";
    String confFile = "src/test/resources/config.properties.test";
    String networkStatusDigest = "rZcFo5tzUMpdy0m3MOegCBqjR0iiHyu59BYneCBtbUk";

    Connection conn = null;
    PsqlConnector psqlConn = new PsqlConnector();
    conn = psqlConn.connect(confFile);

    cp.run(consensusPath, conn);

    PreparedStatement preparedStatement = conn.prepareStatement(
        "SELECT * FROM network_status WHERE digest = '"
        + networkStatusDigest + "'");

    try (ResultSet rs = preparedStatement.executeQuery()) {
      while (rs.next()) {
        assertEquals(rs.getString("digest"), networkStatusDigest);
        assertEquals(rs.getString("consensus_flavor"), "unflavored");
      }
    }
  }
}
