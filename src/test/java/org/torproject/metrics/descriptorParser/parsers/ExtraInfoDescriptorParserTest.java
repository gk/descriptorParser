package org.torproject.metrics.descriptorparser.parsers;

import static junit.framework.TestCase.assertEquals;

import org.torproject.metrics.descriptorparser.utils.PsqlConnector;

import org.junit.Test;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

public class ExtraInfoDescriptorParserTest {

  @Test()
  public void testExtraInfoDescriptorParserDbUploader() throws Exception {
    ExtraInfoDescriptorParser ep = new ExtraInfoDescriptorParser();
    String extraInfoPath =
        "src/test/resources/2022-08-30-16-05-00-extra-infos";
    String confFile = "src/test/resources/config.properties.test";
    String extraInfoDigest = "142ca315888f558a7ab99477fc4928b559988cf5";
    String fingerprint = "BA77149B4EDA76543698F05104F5C2547E306D77";

    Connection conn = null;
    PsqlConnector psqlConn = new PsqlConnector();
    conn = psqlConn.connect(confFile);

    ep.run(extraInfoPath, conn);

    PreparedStatement preparedStatement = conn.prepareStatement(
        "SELECT * FROM extra_info_descriptor WHERE digest_sha1_hex = '"
        + extraInfoDigest + "'");

    try (ResultSet rs = preparedStatement.executeQuery()) {
      while (rs.next()) {
        assertEquals(rs.getString("digest_sha1_hex"), extraInfoDigest);
        assertEquals(rs.getString("fingerprint"),
            fingerprint);
      }
    }

  }

}
